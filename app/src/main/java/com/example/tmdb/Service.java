package com.example.tmdb;

import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.controller.services.ApiService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service {



    private static Retrofit.Builder rerofitBuilder (){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(Credentials.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

    }



    private static Retrofit retrofit = rerofitBuilder().build();
    private static ApiService apiService = retrofit.create(ApiService.class);

    public static ApiService getApiService(){
        return apiService;
    }


}
