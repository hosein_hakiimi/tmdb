package com.example.tmdb.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.tmdb.models.RequestToken;
import com.example.tmdb.repositories.AuthRepository;
import com.example.tmdb.repositories.MovieRepository;

import java.util.List;

public class AuthViewModel extends ViewModel {

    private AuthRepository authRepository;

    public AuthViewModel() {
        authRepository = AuthRepository.getInstance();

    }

    public LiveData<List<RequestToken>> getNewToken(){return authRepository.getNewToken();}

    public LiveData<List<RequestToken>> getLoginData(){return authRepository.getLoginData();}


    public void newToken(Context context){
        authRepository.newToken(context);
    }

    public void login(Context context , String username , String password){
        authRepository.login(context , username , password);
    }
}
