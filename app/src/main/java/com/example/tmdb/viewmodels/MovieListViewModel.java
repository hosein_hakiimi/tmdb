package com.example.tmdb.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.repositories.MovieRepository;

import java.util.List;

//this class is used for view_model

public class MovieListViewModel extends ViewModel {


    // mutable live data is a sub class for a live data (it is act like a live data)
    // Live Data
    //    private MutableLiveData<List<MovieModel>> mMovie ; => move to repository

    private MovieRepository movieRepository;

    public MovieListViewModel() {
        movieRepository = MovieRepository.getInstance();

    }

    public LiveData<List<MovieModel>> getMovie() {
        return movieRepository.getMovies();
    }

    public LiveData<List<MovieModel>> getMoviePop() {
        return movieRepository.getPopular();
    }

    public LiveData<List<MovieModel>> getMovieTopRated() {
        return movieRepository.getTopRated();
    }

    public LiveData<List<MovieModel>> getMovieNowPlaying() {
        return movieRepository.getNowPlaying();
    }

    public LiveData<List<MovieModel>> getMovieRecommendation() {
        return movieRepository.getRecommendation();
    }


    // 3- Calling method in viewModel
    public void searchMovieApi(String query, int pageNumber) {
        movieRepository.searchMovieApi(query, pageNumber);
    }

    public void searchMoviePop(int pageNumber) {
        movieRepository.searchMoviePop(pageNumber);
    }

    public void searchMovieTopRated(int pageNumber) {
        movieRepository.searchMovieTopRated(pageNumber);
    }

    public void searchMovieNowPlaying(int pageNumber) {
        movieRepository.searchMovieNowPlaying(pageNumber);
    }

    public void searchMovieRecommendation(int pageNumber , int movie_id) {
        movieRepository.searchMovieRecommendation(pageNumber , movie_id);
    }


    public void searchNextPage() {
        movieRepository.searchNextPage();
    }

    public void searchNextPage_Popular() {
        movieRepository.searchNextPage_Popular();
    }

    public void searchNextPage_TopRated() {
        movieRepository.searchNextPage_TopRated();
    }

    public void searchNextPage_NowPlaying() {
        movieRepository.searchNextPage_NowPlaying();
    }

    public void searchNextPage_Recommendation() {
        movieRepository.searchNextPage_Recommendation();
    }

}
