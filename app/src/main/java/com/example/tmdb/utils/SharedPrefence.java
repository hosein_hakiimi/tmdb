package com.example.tmdb.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.tmdb.models.RequestToken;

import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class SharedPrefence {

    public static SharedPreferences saveToken(Context context ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("request_token" , Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static SharedPreferences logInToken(Context context ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("main_request_token" , Context.MODE_PRIVATE);
        return sharedPreferences;
    }
    public static SharedPreferences profileData(Context context ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("profile" , Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
