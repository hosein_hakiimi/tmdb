package com.example.tmdb.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tmdb.AppExecutors;
import com.example.tmdb.Service;
import com.example.tmdb.controller.adapter.RecommendationMovieAdapter;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.models.MovieSearchResponse;
import com.example.tmdb.controller.services.Credentials;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieApiClient {

    //Live data for search
    private MutableLiveData<List<MovieModel>> mMovie;
    //Live data for popular movies
    private MutableLiveData<List<MovieModel>> mMoviePop;

    private MutableLiveData<List<MovieModel>> mMovieNowPlaying;

    private MutableLiveData<List<MovieModel>> mMovieTopRated;

    private MutableLiveData<List<MovieModel>> mMovieRecommendation;


    // making global runnable
    private RetrieveMoviesRunnable retrieveMoviesRunnable;
    // making popular runnable
    private RetrieveMoviesRunnablePop retrieveMoviesRunnablePop;

    private RetrieveMoviesRunnableNowPlaying retrieveMoviesRunnableNowPlaying;

    private RetrieveMoviesRunnableTopRated retrieveMoviesRunnableTopRated;

    private RetrieveMoviesRunnableRecommendation retrieveMoviesRunnableRecommendation;


    private MovieApiClient() {
        mMovie = new MutableLiveData<>();
        mMoviePop = new MutableLiveData<>();
        mMovieNowPlaying = new MutableLiveData<>();
        mMovieTopRated = new MutableLiveData<>();
        mMovieRecommendation = new MutableLiveData<>();
    }

    public MutableLiveData<List<MovieModel>> getMovie() {
        return mMovie;
    }

    public MutableLiveData<List<MovieModel>> getMoviePop() {
        return mMoviePop;
    }

    public MutableLiveData<List<MovieModel>> getMovieNowPlaying() {
        return mMovieNowPlaying;
    }

    public MutableLiveData<List<MovieModel>> getMovieTopRated() {
        return mMovieTopRated;
    }

    public MutableLiveData<List<MovieModel>> getMovieRecommendation() {
        return mMovieRecommendation;
    }


    private static MovieApiClient instance;

    public static MovieApiClient getInstance() {
        if (instance == null) {
            instance = new MovieApiClient();
        }
        return instance;
    }


    //1- this method that we are going to call through the classes (put it in the repository and view model and activity)
    public void searchMovieApi(String query, int pageNumber) {

        if (retrieveMoviesRunnable != null) {
            retrieveMoviesRunnable = null;
        }

        retrieveMoviesRunnable = new RetrieveMoviesRunnable(query, pageNumber);


        final Future myHandler = AppExecutors.getInstance().getmNetworkIO().submit(retrieveMoviesRunnable);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //Cancelling the retrofit call
                myHandler.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }

    public void searchMoviePop(int pageNumber) {

        if (retrieveMoviesRunnablePop != null) {
            retrieveMoviesRunnablePop = null;
        }

        retrieveMoviesRunnablePop = new RetrieveMoviesRunnablePop(pageNumber);


        final Future myHandlerPop = AppExecutors.getInstance().getmNetworkIO().submit(retrieveMoviesRunnablePop);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //Cancelling the retrofit call
                myHandlerPop.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }

    public void searchMovieNowPlaying(int pageNumber) {

        if (retrieveMoviesRunnableNowPlaying != null) {
            retrieveMoviesRunnableNowPlaying = null;
        }

        retrieveMoviesRunnableNowPlaying = new RetrieveMoviesRunnableNowPlaying(pageNumber);


        final Future myHandlerNowPlaying = AppExecutors.getInstance().getmNetworkIO().submit(retrieveMoviesRunnableNowPlaying);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                // Cancelling the retrofit call
                myHandlerNowPlaying.cancel(true);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }

    public void searchMovieTopRated(int pageNumber) {

        if (retrieveMoviesRunnableTopRated != null) {
            retrieveMoviesRunnableTopRated = null;
        }

        retrieveMoviesRunnableTopRated = new RetrieveMoviesRunnableTopRated(pageNumber);


        final Future myHandlerTopRated = AppExecutors.getInstance().getmNetworkIO().submit(retrieveMoviesRunnableTopRated);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                // Cancelling the retrofit call
                myHandlerTopRated.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }

    public void searchMovieRecommendation(int pageNumber , int movie_id) {

        if (retrieveMoviesRunnableRecommendation != null) {
            retrieveMoviesRunnableRecommendation = null;
        }

        retrieveMoviesRunnableRecommendation = new RetrieveMoviesRunnableRecommendation(pageNumber , movie_id);


        final Future myHandlerRecommend = AppExecutors.getInstance().getmNetworkIO().submit(retrieveMoviesRunnableRecommendation);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                // Cancelling the retrofit call
                myHandlerRecommend.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }


    //retrieving data from restApi by runnable class
    //we have to types of query : the ID & seach Movie Query
    private class RetrieveMoviesRunnable implements Runnable {

        private String query;
        private int pagenumber;
        boolean cancelRequest;

        public RetrieveMoviesRunnable(String query, int pagenumber) {
            this.query = query;
            this.pagenumber = pagenumber;
            cancelRequest = false;
        }

        @Override
        public void run() {

            //Getting the response objects
            try {
                Response response = getMovies(query, pagenumber).execute();
                if (cancelRequest) {
                    return;
                }
                if (response.code() == 200) {
                    List<MovieModel> list = new ArrayList<>(((MovieSearchResponse) response.body()).getMovies());
                    if (pagenumber == 1) {
                        //sending data to live data
                        // postvalue : used for background thread
                        // setvalue : not for background thread
                        mMovie.postValue(list);
                    } else {
                        List<MovieModel> currentMovies = mMovie.getValue();
                        currentMovies.addAll(list);
                        mMovie.postValue(currentMovies);
                    }
                } else {
                    String error = response.errorBody().string();
                    Log.v("Tag", "error" + error);
                    mMovie.postValue(null);
                }


            } catch (IOException e) {
                e.printStackTrace();
                mMovie.postValue(null);
            }


        }

        //search method query
        private Call<MovieSearchResponse> getMovies(String query, int pagenumber) {
            return Service.getApiService().searchMovie(
                    Credentials.API_KEY,
                    query,
                    pagenumber
            );
        }

        private void CancelRequest() {
            Log.v("Tag", "cancelling search request");
            cancelRequest = true;
        }
    }

    private class RetrieveMoviesRunnablePop implements Runnable {

        private int pagenumber;
        boolean cancelRequest;

        public RetrieveMoviesRunnablePop(int pagenumber) {
            this.pagenumber = pagenumber;
            cancelRequest = false;
        }

        @Override
        public void run() {

            //Getting the response objects
            try {
                Response response2 = getPop(pagenumber).execute();
                if (cancelRequest) {
                    return;
                }
                if (response2.code() == 200) {
                    List<MovieModel> list = new ArrayList<>(((MovieSearchResponse) response2.body()).getMovies());
                    if (pagenumber == 1) {
                        //sending data to live data
                        // postvalue : used for background thread
                        // setvalue : not for background thread
                        mMoviePop.postValue(list);
                    } else {
                        List<MovieModel> currentMovies = mMoviePop.getValue();
                        currentMovies.addAll(list);
                        mMoviePop.postValue(currentMovies);
                    }
                } else {
                    String error = response2.errorBody().string();
                    Log.v("Tag", "error" + error);
                    mMoviePop.postValue(null);
                }


            } catch (IOException e) {
                e.printStackTrace();
                mMoviePop.postValue(null);
            }


        }

        //search method query
        private Call<MovieSearchResponse> getPop(int pagenumber) {
            return Service.getApiService().getPopular(
                    pagenumber,
                    Credentials.API_KEY
            );
        }

        private void CancelRequest() {
            Log.v("Tag", "cancelling search request");
            cancelRequest = true;
        }
    }

    private class RetrieveMoviesRunnableNowPlaying implements Runnable {

        private int pagenumber;
        boolean cancelRequest;

        public RetrieveMoviesRunnableNowPlaying(int pagenumber) {
            this.pagenumber = pagenumber;
            cancelRequest = false;
        }

        @Override
        public void run() {

            //Getting the response objects
            try {
                Response response4 = getNowPlaying(pagenumber).execute();
                if (cancelRequest) {
                    return;
                }
                if (response4.code() == 200) {
                    List<MovieModel> list_NowPlaying = new ArrayList<>(((MovieSearchResponse) response4.body()).getMovies());
                    if (pagenumber == 1) {
                        //sending data to live data
                        // postvalue : used for background thread
                        // setvalue : not for background thread
                        mMovieNowPlaying.postValue(list_NowPlaying);
                    } else {
                        List<MovieModel> currentMovies_NowPlaying = mMovieNowPlaying.getValue();
                        currentMovies_NowPlaying.addAll(list_NowPlaying);
                        mMovieNowPlaying.postValue(currentMovies_NowPlaying);
                    }
                } else {
                    String error = response4.errorBody().string();
                    Log.v("Tag", "error" + error);
                    mMovieNowPlaying.postValue(null);
                }


            } catch (IOException e) {
                e.printStackTrace();
                mMovieNowPlaying.postValue(null);
            }


        }

        //search method query
        private Call<MovieSearchResponse> getNowPlaying(int pagenumber) {
            return Service.getApiService().getNowPlaying(
                    pagenumber,
                    Credentials.API_KEY
            );
        }

        private void CancelRequest() {
            Log.v("Tag", "cancelling search request");
            cancelRequest = true;
        }
    }

    private class RetrieveMoviesRunnableTopRated implements Runnable {

        private int pagenumber;
        boolean cancelRequest;

        public RetrieveMoviesRunnableTopRated(int pagenumber) {
            this.pagenumber = pagenumber;
            cancelRequest = false;
        }

        @Override
        public void run() {

            //Getting the response objects
            try {
                Response response3 = getPop(pagenumber).execute();
                if (cancelRequest) {
                    return;
                }
                if (response3.code() == 200) {
                    List<MovieModel> list_TopRated = new ArrayList<>(((MovieSearchResponse) response3.body()).getMovies());
                    if (pagenumber == 1) {
                        //sending data to live data
                        // postvalue : used for background thread
                        // setvalue : not for background thread
                        mMovieTopRated.postValue(list_TopRated);
                    } else {
                        List<MovieModel> currentMovies_TopRated = mMovieTopRated.getValue();
                        currentMovies_TopRated.addAll(list_TopRated);
                        mMovieTopRated.postValue(currentMovies_TopRated);
                    }
                } else {
                    String error = response3.errorBody().string();
                    Log.v("Tag", "error" + error);
                    mMovieTopRated.postValue(null);
                }


            } catch (IOException e) {
                e.printStackTrace();
                mMovieTopRated.postValue(null);
            }


        }

        //search method query
        private Call<MovieSearchResponse> getPop(int pagenumber) {
            return Service.getApiService().getTopRated(
                    pagenumber,
                    Credentials.API_KEY
            );
        }

        private void CancelRequest() {
            Log.v("Tag", "cancelling search request");
            cancelRequest = true;
        }
    }

    private class RetrieveMoviesRunnableRecommendation implements Runnable {

        private int pagenumber;
        private int movie_id;
        boolean cancelRequest;

        public RetrieveMoviesRunnableRecommendation(int pagenumber, int movie_id) {
            this.pagenumber = pagenumber;
            this.movie_id = movie_id;
            cancelRequest = false;
        }

        @Override
        public void run() {

            //Getting the response objects
            try {
                Response response4 = getRecommendation(pagenumber).execute();
                if (cancelRequest) {
                    return;
                }
                if (response4.code() == 200) {
                    List<MovieModel> list_recommend = (((MovieSearchResponse) response4.body()).getMovies());
                    if (pagenumber == 1) {
                        //sending data to live data
                        // postvalue : used for background thread
                        // setvalue : not for background thread
                        mMovieRecommendation.postValue(list_recommend);
                    } else {
                        List<MovieModel> currentMovies_Recommend = mMovieRecommendation.getValue();
                        currentMovies_Recommend.addAll(list_recommend);
                        mMovieRecommendation.postValue(currentMovies_Recommend);
                    }
                } else {
                    String error = response4.errorBody().string();
                    Log.v("Tag", "error" + error);
                    mMovieRecommendation.postValue(null);
                }


            } catch (IOException e) {
                e.printStackTrace();
                mMovieRecommendation.postValue(null);
            }


        }

        //search method query

        private Call<MovieSearchResponse> getRecommendation(int pagenumber) {
            return Service.getApiService().getMovieRecommendation(
                    movie_id,
                    Credentials.API_KEY,
                    pagenumber
            );
        }

        private void CancelRequest() {
            Log.v("Tag", "cancelling search request");
            cancelRequest = true;
        }
    }


}
