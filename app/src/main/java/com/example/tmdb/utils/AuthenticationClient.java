package com.example.tmdb.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.tmdb.AppExecutors;
import com.example.tmdb.Service;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.models.RequestToken;
import com.example.tmdb.view.activity.MainActivity;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationClient {

    private MutableLiveData<List<RequestToken>> mNewToken;
    private MutableLiveData<List<RequestToken>> mLogin;

    private RetrieveNewTokenRunnable retrieveNewTokenRunnable;
    private RetrieveLoginRunnable retrieveLoginRunnable;

    private AuthenticationClient() {
        mNewToken = new MutableLiveData<>();
        mLogin = new MutableLiveData<>();
    }

    public MutableLiveData<List<RequestToken>> getNewToken() {
        return mNewToken;
    }

    public MutableLiveData<List<RequestToken>> getLoginData() {
        return mLogin;
    }

    private static AuthenticationClient instance;

    public static AuthenticationClient getInstance() {
        if (instance == null) {
            instance = new AuthenticationClient();
        }
        return instance;
    }

    public void newToken(Context context) {

        if (retrieveNewTokenRunnable != null) {
            retrieveNewTokenRunnable = null;
        }

        retrieveNewTokenRunnable = new RetrieveNewTokenRunnable(context);


        final Future myHandler = AppExecutors.getInstance().getmNetworkIO().submit(retrieveNewTokenRunnable);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //Cancelling the retrofit call
                myHandler.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }

    public void login(Context context, String username, String password) {

        if (retrieveLoginRunnable != null) {
            retrieveLoginRunnable = null;
        }

        retrieveLoginRunnable = new RetrieveLoginRunnable(context, username, password);


        final Future myHandler2 = AppExecutors.getInstance().getmNetworkIO().submit(retrieveLoginRunnable);

        // it is for cancelling the call api that user not have crashes in his app
        AppExecutors.getInstance().getmNetworkIO().schedule(new Runnable() {
            @Override
            public void run() {
                //Cancelling the retrofit call
                myHandler2.cancel(false);
            }
        }, 1000, TimeUnit.MILLISECONDS);

    }


    private class RetrieveNewTokenRunnable implements Runnable {

        private Context context;

        public RetrieveNewTokenRunnable(Context context) {
            this.context = context;
        }

        @Override
        public void run() {
            Call<RequestToken> call = Service.getApiService().getNewToken(Credentials.API_KEY);
            call.enqueue(new Callback<RequestToken>() {
                @Override
                public void onResponse(@NonNull Call<RequestToken> call, @NonNull Response<RequestToken> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            save(context, response);
                        }
                    } else {
                        Log.d("RESPONSE", "there is no connection");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RequestToken> call, @NonNull Throwable t) {
                    Log.d("RESPONSE", t.getMessage());
                }
            });
        }
    }

    private class RetrieveLoginRunnable implements Runnable {

        private Context context;
        private String username, password;

        public RetrieveLoginRunnable(Context context, String username, String password) {
            this.context = context;
            this.username = username;
            this.password = password;
        }

        @Override
        public void run() {
            Call<RequestToken> call = Service.getApiService().login(Credentials.API_KEY, username, password, getToken(context));
            call.enqueue(new Callback<RequestToken>() {
                @Override
                public void onResponse(@NonNull Call<RequestToken> call, @NonNull Response<RequestToken> response) {
                        if (response.code() == 200) {
                                Intent intent = new Intent(context, MainActivity.class);
                                save_mainToken(context,response);
                                context.startActivity(intent);
                                ((Activity) context).finish();

                        } else {
                            if (response.code() == 401){
                                Toast.makeText(context, "Invalid username or password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                @Override
                public void onFailure(@NonNull Call<RequestToken> call, @NonNull Throwable t) {
                    Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static void save(Context context, Response<RequestToken> response) {

        SharedPreferences.Editor editor = SharedPrefence.saveToken(context).edit();
        editor.putString("token", response.body().getRequest_token());
        editor.apply();

    }

    public static void save_mainToken(Context context, Response<RequestToken> response) {
        SharedPreferences.Editor editor = SharedPrefence.logInToken(context).edit();
        editor.putString("main_token", response.body().getRequest_token());
        editor.putString("status", "true");
        editor.apply();
    }

    public static String getToken(Context context) {
        return SharedPrefence.saveToken(context).getString("token", "");
    }


}





