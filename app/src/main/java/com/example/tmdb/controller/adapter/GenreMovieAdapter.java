package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tmdb.databinding.ItemGenreFragmentMovieDetailBinding;
import com.example.tmdb.models.MovieDetail.GenreMovie;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class GenreMovieAdapter extends RecyclerView.Adapter<GenreMovieAdapter.ViewHolder> {

    private List<GenreMovie> genreMovieList;
    private Context context;

    public GenreMovieAdapter(List<GenreMovie> genreMovieList, Context context) {
        this.genreMovieList = genreMovieList;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public GenreMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemGenreFragmentMovieDetailBinding.inflate(LayoutInflater.from(context) , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull GenreMovieAdapter.ViewHolder holder, int position) {
        GenreMovie genreMovie = genreMovieList.get(position);

        holder.binding.textviewGenreItemGenre.setText(genreMovie.getName());

    }

    @Override
    public int getItemCount() {
        if (genreMovieList != null){
            return genreMovieList.size();
        }else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemGenreFragmentMovieDetailBinding binding;

        public ViewHolder(@NonNull @NotNull ItemGenreFragmentMovieDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
