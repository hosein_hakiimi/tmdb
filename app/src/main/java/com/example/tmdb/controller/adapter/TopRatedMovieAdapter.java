package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.ItemMovieTopRatedFragmentHomeBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.utils.SharedPrefence;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TopRatedMovieAdapter extends RecyclerView.Adapter<TopRatedMovieAdapter.ViewHolder> {

    private Context context;
    private List<MovieModel> mMovie_Top_Rated;

    public TopRatedMovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public TopRatedMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemMovieTopRatedFragmentHomeBinding.inflate(LayoutInflater.from(context) , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TopRatedMovieAdapter.ViewHolder holder, int position) {

        MovieModel movieModel = mMovie_Top_Rated.get(position);

        Glide.with(context).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewItemMovieTopRatedFragmentHome);

        holder.binding.textviewItemMovieTopRatedFragmentHome.setText(String.valueOf(movieModel.getVote_average()));


        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id" , movieModel.getId());


            Navigation.findNavController(v).navigate(R.id.movieDetailFragment , bundle);
        });

    }

    @Override
    public int getItemCount() {
        if (mMovie_Top_Rated != null) {
            return mMovie_Top_Rated.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemMovieTopRatedFragmentHomeBinding binding;

        public ViewHolder(@NonNull @NotNull ItemMovieTopRatedFragmentHomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void setmMovie_Top_Rated(List<MovieModel> mMovie_Top_Rated){
        this.mMovie_Top_Rated = mMovie_Top_Rated;
        notifyDataSetChanged();

    }
}
