package com.example.tmdb.controller.services;

import com.example.tmdb.models.MovieDetail.ImagesMovie;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.models.MovieSearchResponse;
import com.example.tmdb.models.RequestToken;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {



    // search for movie
    @GET("3/search/movie")
    Call<MovieSearchResponse> searchMovie(
            @Query("api_key") String key,
            @Query("query") String query,
            @Query("page") int page
    );

    // making search with id
    @GET("3/movie/{movie_id}?")
    Call<MovieModel> getMovie(
            @Path("movie_id") int movie_id,
            @Query("api_key") String api_key
    );

    @GET("3/movie/popular?")
    Call<MovieSearchResponse> getPopular(
            @Query("page") int page,
            @Query("api_key") String api_key
    );

    @GET("3/movie/top_rated?")
    Call<MovieSearchResponse> getTopRated(
            @Query("page") int page,
            @Query("api_key") String api_key
    );

    @GET("3/movie/now_playing?")
    Call<MovieSearchResponse> getNowPlaying(
            @Query("page") int page,
            @Query("api_key") String api_key
    );

    @GET("3/authentication/token/new?")
    Call<RequestToken> getNewToken(
            @Query("api_key") String api_key
    );

    @POST("3/authentication/token/validate_with_login?")
    Call<RequestToken> login(
            @Query("api_key") String api_key,
            @Query("username") String username,
            @Query("password") String password,
            @Query("request_token") String request_token

    );

    @GET("3/movie/{movie_id}?")
    Call<MovieModel> getMovieDetail(
            @Path("movie_id") int movie_id,
            @Query("api_key") String api_key
    );

    @GET("3/movie/{movie_id}/images?")
    Call<ImagesMovie> getMovieImages(
            @Path("movie_id") int movie_id,
            @Query("api_key") String api_key
    );

    @GET("3/movie/{movie_id}/recommendations?")
    Call<MovieSearchResponse> getMovieRecommendation(
            @Path("movie_id") int movie_id,
            @Query("api_key") String api_key,
            @Query("page") int page
    );

}
