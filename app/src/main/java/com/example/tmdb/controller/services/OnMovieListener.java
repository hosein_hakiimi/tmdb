package com.example.tmdb.controller.services;

public interface OnMovieListener {

    void onMovieClick(int position);
    void onCategoryClick(String category);
}
