package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentDetailBinding;
import com.example.tmdb.databinding.ItemMoviePopularFragmentHomeBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.utils.SharedPrefence;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PopularMovieAdapter extends RecyclerView.Adapter<PopularMovieAdapter.ViewHolder> {

    private Context context;
    private List<MovieModel> mMovie;

    public PopularMovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public PopularMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemMoviePopularFragmentHomeBinding.inflate(LayoutInflater.from(parent.getContext()), parent
                , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PopularMovieAdapter.ViewHolder holder, int position) {

        MovieModel movieModel = mMovie.get(position);

//        holder.binding.textviewTitleItemRecyclerviewMovieListItem.setText(movieModel.getTitle());
//
//        holder.binding.textviewLanguageItemRecyclerviewMovieListItem.setText(movieModel.getOriginal_language());
//
//        holder.binding.textviewCategoryItemRecyclerviewMovieListItem.setText(String.valueOf(movieModel.getRelease_date()));

        // vote average is over 10 , and our ratiing bar is over 5 stars : dividing by 2

        Glide.with(context).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewPopularItemRecyclerviewMovieListItem);


        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id", movieModel.getId());


//            Intent intent = new Intent(v.getContext(), MovieDetailActivity.class);
//            intent.putExtras(bundle);
////            intent.putExtra("movie" , getSelectedMovie(position));
//            v.getContext().startActivity(intent);


            Navigation.findNavController(v).navigate(R.id.movieDetailFragment, bundle);
        });

    }

    @Override
    public int getItemCount() {
        if (mMovie != null) {
            return mMovie.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemMoviePopularFragmentHomeBinding binding;

        public ViewHolder(@NonNull @NotNull ItemMoviePopularFragmentHomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void setmMovies(List<MovieModel> mMovies) {
        this.mMovie = mMovies;
        notifyDataSetChanged();
    }
}
