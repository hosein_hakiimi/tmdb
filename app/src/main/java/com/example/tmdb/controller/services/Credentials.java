package com.example.tmdb.controller.services;

public class Credentials {

    public static final String BASE_URL = "https://api.themoviedb.org/";

    public static final String API_KEY = "b8dd19559ebbbadf3c31009fa3f093cb";

    public static final String BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500";

    public static final String BASE_GRANTE_TOKEN_URL = "https://www.themoviedb.org/authenticate/";

    public static final boolean POPULAR = false;

}
