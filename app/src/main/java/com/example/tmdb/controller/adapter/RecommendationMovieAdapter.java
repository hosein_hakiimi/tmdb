package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.ItemGenreFragmentMovieDetailBinding;
import com.example.tmdb.databinding.ItemMovieNowPlayingFragmentHomeBinding;
import com.example.tmdb.databinding.ItemRecyclerviewFragmentRecommendationnBinding;
import com.example.tmdb.models.MovieDetail.GenreMovie;
import com.example.tmdb.models.MovieDetail.MovieModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RecommendationMovieAdapter extends RecyclerView.Adapter<RecommendationMovieAdapter.ViewHolder> {

    private List<MovieModel> movieModelList;
    private Context context;

    public RecommendationMovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public RecommendationMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemRecyclerviewFragmentRecommendationnBinding.inflate(LayoutInflater.from(context) , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecommendationMovieAdapter.ViewHolder holder, int position) {
        MovieModel movieModel = movieModelList.get(position);

        holder.binding.textviewItemMovieNowPlayingFragmentRecommendation.setText(movieModel.getTitle());
        Glide.with(context).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewItemMovieNowPlayingFragmentRecommendation);

        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id", movieModel.getId());

            Navigation.findNavController(v).navigate(R.id.movieDetailFragment, bundle);
        });

    }

    @Override
    public int getItemCount() {
        if (movieModelList != null){
            return movieModelList.size();
        }else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemRecyclerviewFragmentRecommendationnBinding binding;

        public ViewHolder(@NonNull @NotNull ItemRecyclerviewFragmentRecommendationnBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
    public void setmMovie_Recommendation(List<MovieModel> movieModelList) {
        this.movieModelList = movieModelList;
        notifyDataSetChanged();
    }
}
