package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.MovieListItemBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private Context context;
    private List<MovieModel> mMovie;

    private static final int DISPLAY_POP = 1;
    private static final int DISPLAY_SARCH = 2;

    public MovieAdapter(Context context) {

        this.context = context;
//        this.onMovieListener = onMovieListener;
    }

    @NonNull
    @NotNull
    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if (viewType == DISPLAY_SARCH) {
            return new ViewHolder(MovieListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent
                    , false));
        } else {
            return new ViewHolder(MovieListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent
                    , false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MovieAdapter.ViewHolder holder, int position) {

        int itemView_Type = getItemViewType(position);
        MovieModel movieModel = mMovie.get(position);

        if (itemView_Type == DISPLAY_SARCH) {
            ((ViewHolder) holder).binding.textviewTitleItemRecyclerviewMovieListItem.setText(movieModel.getTitle());

            ((ViewHolder) holder).binding.textviewLanguageItemRecyclerviewMovieListItem.setText(movieModel.getOriginal_language());

            ((ViewHolder) holder).binding.textviewCategoryItemRecyclerviewMovieListItem.setText(String.valueOf(movieModel.getRelease_date()));

            // vote average is over 10 , and our ratiing bar is over 5 stars : dividing by 2
            ((ViewHolder) holder).binding.ratingBarItemRecyclerviewMovieListItem.setRating(movieModel.getVote_average() / 2);

            Glide.with(holder.itemView.getContext()).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewItemRecyclerviewMovieListItem);

        } else {
            ((ViewHolder) holder).binding.textviewTitleItemRecyclerviewMovieListItem.setText(movieModel.getTitle());

            ((ViewHolder) holder).binding.textviewLanguageItemRecyclerviewMovieListItem.setText(movieModel.getOriginal_language());

            ((ViewHolder) holder).binding.textviewCategoryItemRecyclerviewMovieListItem.setText(String.valueOf(movieModel.getRelease_date()));

            // vote average is over 10 , and our ratiing bar is over 5 stars : dividing by 2
            ((ViewHolder) holder).binding.ratingBarItemRecyclerviewMovieListItem.setRating(movieModel.getVote_average() / 2);

            Glide.with(holder.itemView.getContext()).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewItemRecyclerviewMovieListItem);

        }


//        holder.itemView.setOnClickListener(v -> {
//            Toast.makeText(v.getContext(), "The position is :" +position, Toast.LENGTH_SHORT).show();
//        });

        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("poster_path", Credentials.BASE_IMAGE_URL + movieModel.getPoster_path());
            bundle.putString("title", movieModel.getTitle());
            bundle.putFloat("vote_average", movieModel.getVote_average() / 2);
            bundle.putString("overview", movieModel.getOverview());

//            Intent intent = new Intent(v.getContext(), MovieDetailActivity.class);
//            intent.putExtras(bundle);
////            intent.putExtra("movie" , getSelectedMovie(position));
//            v.getContext().startActivity(intent);

        });
    }

    public void setmMovies(List<MovieModel> mMovies) {
        this.mMovie = mMovies;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mMovie != null) {
            return mMovie.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private MovieListItemBinding binding;

        public ViewHolder(@NonNull @NotNull MovieListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }


    public MovieModel getSelectedMovie(int position) {
        if (mMovie != null) {
            if (mMovie.size() > 0) {
                return mMovie.get(position);
            }
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (Credentials.POPULAR) {
            return DISPLAY_POP;
        } else {
            return DISPLAY_SARCH;
        }
    }
}
