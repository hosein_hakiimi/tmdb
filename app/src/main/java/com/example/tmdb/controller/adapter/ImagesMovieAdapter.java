package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.ItemGenreFragmentMovieDetailBinding;
import com.example.tmdb.databinding.ItemImagesFragmentMovieDetailBinding;
import com.example.tmdb.models.MovieDetail.BackdropsImage;
import com.example.tmdb.models.MovieDetail.GenreMovie;
import com.example.tmdb.models.MovieDetail.ImagesMovie;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ImagesMovieAdapter extends RecyclerView.Adapter<ImagesMovieAdapter.ViewHolder> {

    private List<BackdropsImage> backdropsImages;
    private Context context;

    public ImagesMovieAdapter(List<BackdropsImage> backdropsImages, Context context) {
        this.backdropsImages = backdropsImages;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public ImagesMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemImagesFragmentMovieDetailBinding.inflate(LayoutInflater.from(context) , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ImagesMovieAdapter.ViewHolder holder, int position) {
        BackdropsImage backdropsImage = backdropsImages.get(position);

        if (backdropsImage.getIso_639_1() == null){
            Glide.with(context).load(Credentials.BASE_IMAGE_URL + backdropsImage.getFile_path()).into(holder.binding.imageviewItemImages);

        }
    }

    @Override
    public int getItemCount() {
        if (backdropsImages != null){
            return backdropsImages.size();
        }else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemImagesFragmentMovieDetailBinding binding;

        public ViewHolder(@NonNull @NotNull ItemImagesFragmentMovieDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
