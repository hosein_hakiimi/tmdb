package com.example.tmdb.controller.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.ItemMovieNowPlayingFragmentHomeBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.utils.SharedPrefence;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NowPlayingMovieAdapter extends RecyclerView.Adapter<NowPlayingMovieAdapter.ViewHolder> {

    private Context context;
    private List<MovieModel> mMovie_Now_Playing;

    public NowPlayingMovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public NowPlayingMovieAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemMovieNowPlayingFragmentHomeBinding.inflate(LayoutInflater.from(context) , parent , false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull NowPlayingMovieAdapter.ViewHolder holder, int position) {

        MovieModel movieModel = mMovie_Now_Playing.get(position);

        Glide.with(context).load(Credentials.BASE_IMAGE_URL + movieModel.getPoster_path()).into(holder.binding.imageviewItemMovieNowPlayingFragmentHome);

        holder.binding.textviewItemMovieNowPlayingFragmentHome.setText(movieModel.getTitle());

        holder.itemView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("movie_id" , movieModel.getId());


            Navigation.findNavController(v).navigate(R.id.movieDetailFragment , bundle);
        });

    }

    @Override
    public int getItemCount() {
        if (mMovie_Now_Playing != null) {
            return mMovie_Now_Playing.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemMovieNowPlayingFragmentHomeBinding binding;

        public ViewHolder(@NonNull @NotNull ItemMovieNowPlayingFragmentHomeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void setmMovie_Now_Playing(List<MovieModel> mMovie_Top_Rated){
        this.mMovie_Now_Playing = mMovie_Top_Rated;
        notifyDataSetChanged();

    }
}
