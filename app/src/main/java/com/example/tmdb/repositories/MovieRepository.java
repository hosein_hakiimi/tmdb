package com.example.tmdb.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.utils.MovieApiClient;

import java.util.List;

public class MovieRepository {

    // This class is acting as repositories


    //Live data
//    private MutableLiveData<List<MovieModel>> mMovie ; => move to movie api client


    // in ordee to link with MovieApiClient
    private MovieApiClient movieApiClient;

    private MovieRepository() {
        movieApiClient = MovieApiClient.getInstance();
    }


    private static MovieRepository instance;

    public static MovieRepository getInstance() {

        if (instance == null) {
            instance = new MovieRepository();
        }
        return instance;
    }


    private String mQuery;
    private int mPageNumber;
    private int mMovie_id;


    public LiveData<List<MovieModel>> getMovies() {
        return movieApiClient.getMovie();
    }

    public LiveData<List<MovieModel>> getPopular() {
        return movieApiClient.getMoviePop();
    }

    public LiveData<List<MovieModel>> getTopRated() {
        return movieApiClient.getMovieTopRated();
    }

    public LiveData<List<MovieModel>> getNowPlaying() {
        return movieApiClient.getMovieNowPlaying();
    }

    public LiveData<List<MovieModel>> getRecommendation() {
        return movieApiClient.getMovieRecommendation();
    }

    // we need to call the search movie that it is in the movie api in the repository and then we call the
    // method of the repostiroy in the ViewModel and the method of the ViewModel in to a Activity

    // 2- calling the method in the repository
    public void searchMovieApi(String query, int pageNumber) {
        mQuery = query;
        mPageNumber = pageNumber;
        movieApiClient.searchMovieApi(query, pageNumber);

    }

    public void searchMoviePop(int pageNumber) {
        mPageNumber = pageNumber;
        movieApiClient.searchMoviePop(pageNumber);

    }

    public void searchMovieTopRated(int pageNumber) {
        mPageNumber = pageNumber;
        movieApiClient.searchMovieTopRated(pageNumber);
    }

    public void searchMovieNowPlaying(int pageNumber) {
        mPageNumber = pageNumber;
        movieApiClient.searchMovieNowPlaying(pageNumber);
    }

    public void searchMovieRecommendation(int pageNumber ,int movie_id) {
        mMovie_id = movie_id;
        mPageNumber = pageNumber;
        movieApiClient.searchMovieRecommendation(pageNumber , movie_id);
    }


    public void searchNextPage() {
        searchMovieApi(mQuery, mPageNumber + 1);
    }

    public void searchNextPage_Popular() {
        searchMoviePop(mPageNumber + 1);
    }

    public void searchNextPage_TopRated() {
        searchMovieTopRated(mPageNumber + 1);
    }

    public void searchNextPage_NowPlaying() {
        searchMovieNowPlaying(mPageNumber + 1);
    }

    public void searchNextPage_Recommendation() {
        searchMovieRecommendation(mPageNumber + 1 , mMovie_id);
    }

}
