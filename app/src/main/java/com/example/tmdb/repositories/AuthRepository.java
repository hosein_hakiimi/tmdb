package com.example.tmdb.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.example.tmdb.models.RequestToken;
import com.example.tmdb.utils.AuthenticationClient;

import java.util.List;

public class AuthRepository {
    private AuthenticationClient authenticationClient;

    private AuthRepository(){
        authenticationClient = AuthenticationClient.getInstance();}

    private static AuthRepository instance;
    public static AuthRepository getInstance(){
        if (instance == null){
            instance = new AuthRepository();
        }return instance;
    }

    public LiveData<List<RequestToken>> getNewToken(){return authenticationClient.getNewToken();}

    public LiveData<List<RequestToken>> getLoginData(){return authenticationClient.getLoginData();}


    public void newToken(Context context){
        authenticationClient.newToken(context);
    }

    public void login(Context context , String username , String password){
        authenticationClient.login(context , username , password);
    }
}
