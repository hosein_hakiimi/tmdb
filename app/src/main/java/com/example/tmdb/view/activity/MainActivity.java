package com.example.tmdb.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.Navigation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.tmdb.R;
import com.example.tmdb.databinding.ActivityMainBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.Service;
import com.example.tmdb.models.MovieSearchResponse;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.controller.services.ApiService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    // before we run our app , we need to add the network security configuration
    // because we need to allow our retrofit to configurate the sub domains
    // we wrote the codes in xml => network_security_config

    private ActivityMainBinding binding;

    // ViewModel

//    boolean isPopular = true;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        onClick();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//        movieListViewModel =  new ViewModelProvider(this).get(MovieListViewModel.class);


        // Getting popular movies
//        movieListViewModel.searchMoviePop(1);

        // testing the method
//        binding.buttonMainActivity.setOnClickListener(v -> {
//            // displaying only the result of page 1
//            searchMovieApi("Fast" , 1);
//        });

    }

    private void onClick(){
        binding.constraintSearchActivityMain.setOnClickListener(v -> {
            Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment_main).navigate(R.id.searchFragment);
            binding.imageviewSearchActivityMain.setColorFilter(getResources().getColor(R.color.tosi1));
            binding.imageviewHomeActivityMain.setColorFilter(getResources().getColor(R.color.black));
            binding.imageviewProfleActivityMain.setColorFilter(getResources().getColor(R.color.black));
        });
        binding.constraintHomeActivityMain.setOnClickListener(v -> {
            Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment_main).navigate(R.id.homeFragment);
            binding.imageviewSearchActivityMain.setColorFilter(getResources().getColor(R.color.black));
            binding.imageviewHomeActivityMain.setColorFilter(getResources().getColor(R.color.tosi1));
            binding.imageviewProfleActivityMain.setColorFilter(getResources().getColor(R.color.black));
        });
        binding.constraintProfileActivityMain.setOnClickListener(v -> {
            Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment_main).navigate(R.id.profileFragment);
            binding.imageviewSearchActivityMain.setColorFilter(getResources().getColor(R.color.black));
            binding.imageviewHomeActivityMain.setColorFilter(getResources().getColor(R.color.black));
            binding.imageviewProfleActivityMain.setColorFilter(getResources().getColor(R.color.tosi1));
        });
//        binding.logout.setOnClickListener(view -> {
//            logOut();
//        });
    }


//    private void ObservePopularMovies() {
//
//        movieListViewModel.getMoviePop().observe(this, new Observer<List<MovieModel>>() {
//            @Override
//            public void onChanged(List<MovieModel> movieModels2) {
//                // Observing for any data change
//                if (movieModels2 != null){
//                    for (MovieModel movieModel2 : movieModels2){
////                        Log.v("Tagy" , "onChanged:" + movieModel2.getTitle());
//
//                        movieAdapter.setmMovies(movieModels2);
//                    }
//                }
//
//            }
//        });
//
//
//    }






    // 4- calling method in main activity
//    private void searchMovieApi(String query, int pageNumber){
//        movieListViewModel.searchMovieApi(query,pageNumber);
//    }


    // 5- intializing recyclerview $ adding data to it
//    private void configureRecycleview(){
//
//        movieAdapter = new MovieAdapter(getApplicationContext());
//        binding.recyclerviewActivityMain.setAdapter(movieAdapter);
//        binding.recyclerviewActivityMain.setLayoutManager(new LinearLayoutManager(this ));
//
//        // recyclerview pagination
//        // loading next page of api response
//        binding.recyclerviewActivityMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
//                if (!recyclerView.canScrollVertically(1)){
//                    // Here we need to display the next search result on the next page of api
//                    movieListViewModel.searchNextPage();
//                }
//            }
//        });
//
//    }



    // not using it but have the code is not bad
    private void GetRetrofitResponse() {
        ApiService apiService = Service.getApiService();

        Call<MovieSearchResponse> movieSearchResponseCal = apiService
                .searchMovie(
                        Credentials.API_KEY,
                        "action",
                        1
                );
        movieSearchResponseCal.enqueue(new Callback<MovieSearchResponse>() {
            @Override
            public void onResponse(Call<MovieSearchResponse> call, Response<MovieSearchResponse> response) {
                if (response.code() == 200) {
                    Log.v("Tag", "the response" + response.body().toString());

                    List<MovieModel> movies = new ArrayList<>(response.body().getMovies());
                    for (MovieModel movie : movies) {
                        Log.v("Tag", "The Movie release date" + movie.getRelease_date());
                    }
                } else {
                    try {
                        Log.v("Tag", "Error" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieSearchResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void GetRetrofitResponseAccordingToID() {
        ApiService apiService = Service.getApiService();
        Call<MovieModel> movieModelResponseCall = apiService.getMovie(
                550,
                Credentials.API_KEY
        );

        movieModelResponseCall.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(Call<MovieModel> call, Response<MovieModel> response) {
                // this response code is mean that it is success
                if (response.code() == 200){
                    MovieModel movieModel = response.body();
                    Log.v("Tag", "the response" + movieModel.getTitle());
                }else {
                    try {
                        Log.v("Tag" ,"Error" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieModel> call, Throwable t) {

            }
        });

    }


}