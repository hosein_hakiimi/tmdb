package com.example.tmdb.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.Service;
import com.example.tmdb.controller.adapter.GenreMovieAdapter;
import com.example.tmdb.controller.adapter.ImagesMovieAdapter;
import com.example.tmdb.controller.adapter.PopularMovieAdapter;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentDetailBinding;
import com.example.tmdb.models.MovieDetail.BackdropsImage;
import com.example.tmdb.models.MovieDetail.GenreMovie;
import com.example.tmdb.models.MovieDetail.ImagesMovie;
import com.example.tmdb.models.MovieDetail.LanguageMovie;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.models.MovieDetail.ProductionCompany;
import com.example.tmdb.models.MovieDetail.ProductionCountryMovie;
import com.example.tmdb.utils.SharedPrefence;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment {

    private FragmentDetailBinding binding;

    private List<ProductionCompany> productionCompanyList;
    private List<LanguageMovie> languageMovieList;

    private List<GenreMovie> genreMovieList;
    GenreMovieAdapter genreMovieAdapter;

    private List<BackdropsImage> backdropsImageList;
    ImagesMovieAdapter imagesMovieAdapter;

    private PopularMovieAdapter popularMovieAdapter;


    int postion;


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater, container, false);

        getDetail();

        return binding.getRoot();
    }

    private void getDetail() {


        Bundle bundle = getArguments();
        int movie_idd = bundle.getInt("id_movie");

        Call<MovieModel> call = Service.getApiService().getMovieDetail(movie_idd, Credentials.API_KEY);
        call.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(@NotNull Call<MovieModel> call, @NotNull Response<MovieModel> response) {
                //get Company
                productionCompanyList = response.body().getProductionCompanies();
                ProductionCompany productionCompany = productionCompanyList.get(postion);

                //get language
                languageMovieList = response.body().getLanguageMovies();
                LanguageMovie languageMovie = languageMovieList.get(postion);

                //get production country
                ProductionCountryMovie productionCountryMovie = response.body().getProductionCountryMovies().get(postion);

                // get genre
                genreMovieList = response.body().getGenreMovies();
                genreMovieAdapter = new GenreMovieAdapter(genreMovieList, getContext());
                binding.recyclerviewGenreFragmentDetail.setAdapter(genreMovieAdapter);
                binding.recyclerviewGenreFragmentDetail.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));


                binding.textviewTitleFragmentDetail.setText(response.body().getTitle());
                binding.textviewDescriptionFragmentDetail.setText(response.body().getOverview());
                binding.ratingBarFragmentDetail.setRating(response.body().getVote_average() / 2);
                binding.textviewVoteAverageFragmentDetail.setText(String.valueOf(response.body().getVote_average()));
                binding.textviewLanguageAfragmentDetail.setText(languageMovie.getEnglish_name());
                binding.textviewVoteCountFragmentDetail.setText(String.valueOf(response.body().getVote_count()));
                binding.textviewStudioFragmentDetail.setText(productionCompany.getName());
                binding.textviewReleaseDateFragmentDetail.setText(response.body().getRelease_date());
                binding.textviewStatusFragmentDetail.setText(response.body().getStatus());
                binding.textviewCountryFragmentDetail.setText(productionCountryMovie.getName());

            }


            @Override
            public void onFailure(@NotNull Call<MovieModel> call, @NotNull Throwable t) {

            }
        });

        Call<ImagesMovie> imagesMovieCall = Service.getApiService().getMovieImages(movie_idd, Credentials.API_KEY);
        imagesMovieCall.enqueue(new Callback<ImagesMovie>() {
            @Override
            public void onResponse(@NotNull Call<ImagesMovie> call, @NotNull Response<ImagesMovie> response) {

                backdropsImageList = response.body().getBackdropsImages();
                imagesMovieAdapter = new ImagesMovieAdapter(backdropsImageList, getContext());
                binding.recyclerviewIamgesFragmentDetail.setAdapter(imagesMovieAdapter);
                binding.recyclerviewIamgesFragmentDetail.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));


            }

            @Override
            public void onFailure(@NotNull Call<ImagesMovie> call, @NotNull Throwable t) {

            }
        });


    }


}