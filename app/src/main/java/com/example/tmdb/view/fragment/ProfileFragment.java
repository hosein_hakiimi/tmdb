package com.example.tmdb.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmdb.R;
import com.example.tmdb.databinding.FragmentProfileBinding;
import com.example.tmdb.utils.SharedPrefence;
import com.example.tmdb.view.activity.AuthActivity;
import com.example.tmdb.viewmodels.AuthViewModel;


public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    AuthViewModel authViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater , container , false);

        getProfileData();
        onClick();
        getToken();

        return binding.getRoot();
    }
    private void getProfileData(){
        String username = SharedPrefence.profileData(getContext()).getString("username" , "");
        binding.textviewUsernameFragmentProfile.setText(username);
    }

    private void onClick(){
        binding.logout.setOnClickListener(view -> {
            logOut();
        });

    }

    private void logOut(){
        sharedPreferences = getContext().getSharedPreferences("main_request_token" , Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Intent intent = new Intent(getContext() , AuthActivity.class);
        startActivity(intent);
        ((Activity)getContext()).finish();
        editor.putString("main_token" , "");
        editor.apply();
    }

    private void getToken(){
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        authViewModel.getNewToken();
        authViewModel.newToken(getContext());
    }
}