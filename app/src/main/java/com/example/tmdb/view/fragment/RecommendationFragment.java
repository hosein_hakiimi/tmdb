package com.example.tmdb.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmdb.R;
import com.example.tmdb.Service;
import com.example.tmdb.controller.adapter.GenreMovieAdapter;
import com.example.tmdb.controller.adapter.ImagesMovieAdapter;
import com.example.tmdb.controller.adapter.RecommendationMovieAdapter;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentRecommendationBinding;
import com.example.tmdb.models.MovieDetail.BackdropsImage;
import com.example.tmdb.models.MovieDetail.GenreMovie;
import com.example.tmdb.models.MovieDetail.ImagesMovie;
import com.example.tmdb.models.MovieDetail.LanguageMovie;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.models.MovieDetail.ProductionCompany;
import com.example.tmdb.models.MovieDetail.ProductionCountryMovie;
import com.example.tmdb.viewmodels.MovieListViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecommendationFragment extends Fragment {

    MovieListViewModel movieListViewModel;
    RecommendationMovieAdapter recommendationMovieAdapter;
    int pos;



    private FragmentRecommendationBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRecommendationBinding.inflate(inflater , container ,false);

        observList();
        configureRecyclerview();

        return binding.getRoot();
    }

    private void configureRecyclerview(){
        recommendationMovieAdapter = new RecommendationMovieAdapter(getContext());
        binding.reyclerviewRecommend.setAdapter(recommendationMovieAdapter);
        binding.reyclerviewRecommend.setLayoutManager(new GridLayoutManager(getContext() , 3));

        binding.reyclerviewRecommend.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)){
                    movieListViewModel.searchNextPage_Recommendation();
                }
            }
        });

    }
    private void observList(){

        Bundle bundle = getArguments();
        int movie_id = bundle.getInt("id_movie");

        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);
        movieListViewModel.getMovieRecommendation().observe(getViewLifecycleOwner(), new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels) {
                if (movieModels != null){
                    recommendationMovieAdapter.setmMovie_Recommendation(movieModels);
                }
            }
        });
        movieListViewModel.searchMovieRecommendation(1  , movie_id);

    }


}