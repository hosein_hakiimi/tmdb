package com.example.tmdb.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmdb.controller.adapter.NowPlayingMovieAdapter;
import com.example.tmdb.controller.adapter.PopularMovieAdapter;
import com.example.tmdb.controller.adapter.TopRatedMovieAdapter;
import com.example.tmdb.databinding.FragmentHomeBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.viewmodels.MovieListViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    private MovieListViewModel movieListViewModel;

    private PopularMovieAdapter popularMovieAdapter;
    private TopRatedMovieAdapter topRatedMovieAdapter;
    private NowPlayingMovieAdapter nowPlayingMovieAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false);

        configureRecycleview();
        observeMovies();


        return binding.getRoot();
    }

    private void configureRecycleview() {

        //rec popular
        popularMovieAdapter = new PopularMovieAdapter(getContext());
        binding.recyclerviewFragmentHome.setAdapter(popularMovieAdapter);
        binding.recyclerviewFragmentHome.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.HORIZONTAL,false));

        // recyclerview pagination
        // loading next page of api response
        binding.recyclerviewFragmentHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollHorizontally(1)) {
                    // Here we need to display the next search result on the next page of api
                    movieListViewModel.searchNextPage_Popular();
                }
            }
        });

        //rec top rated
        topRatedMovieAdapter = new TopRatedMovieAdapter(getContext());
        binding.recyclerviewTopRatedFragmentHome.setAdapter(topRatedMovieAdapter);
        binding.recyclerviewTopRatedFragmentHome.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.HORIZONTAL,false));

        // recyclerview pagination
        // loading next page of api response
        binding.recyclerviewTopRatedFragmentHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollHorizontally(1)) {
                    // Here we need to display the next search result on the next page of api
                    movieListViewModel.searchNextPage_TopRated();
                }
            }
        });

        //rec Now Playing
        nowPlayingMovieAdapter = new NowPlayingMovieAdapter(getContext());
        binding.recyclerviewNowPlayingFragmentHome.setAdapter(nowPlayingMovieAdapter);
        binding.recyclerviewNowPlayingFragmentHome.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.HORIZONTAL,false));

        // recyclerview pagination
        // loading next page of api response
        binding.recyclerviewNowPlayingFragmentHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollHorizontally(1)) {
                    // Here we need to display the next search result on the next page of api
                    movieListViewModel.searchNextPage_NowPlaying();
                }
            }
        });
    }

    private void observeMovies() {
        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        // get Movie Popular
        movieListViewModel.getMoviePop().observe(getViewLifecycleOwner(), new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels) {
                // Observing for any data change
                if (movieModels != null) {
//                    for (MovieModel movieModel : movieModels) {


                    binding.shimmerItemMoviePopularFragmentHome.stopShimmer();
                    binding.shimmerItemMoviePopularFragmentHome.setVisibility(View.GONE);

                    popularMovieAdapter.setmMovies(movieModels);
//                    }
                }

            }
        });
        movieListViewModel.searchMoviePop(1);


        // get Movie Top Rated
        movieListViewModel.getMovieTopRated().observe(getViewLifecycleOwner(), new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels_TopRated) {
                // Observing for any data change
                if (movieModels_TopRated != null) {
//                    for (MovieModel movieModel : movieModels) {


                    binding.shimmerItemMovieTopRatedFragmentHome.stopShimmer();
                    binding.shimmerItemMovieTopRatedFragmentHome.setVisibility(View.GONE);

                    topRatedMovieAdapter.setmMovie_Top_Rated(movieModels_TopRated);
//                    }
                }

            }
        });
        movieListViewModel.searchMovieTopRated(1);


        // get Movie Now Playing
        movieListViewModel.getMovieNowPlaying().observe(getViewLifecycleOwner(), new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels_NowPlaying) {
                // Observing for any data change
                if (movieModels_NowPlaying != null) {
//                    for (MovieModel movieModel : movieModels) {

                    binding.shimmerItemMovieNowPlayingFragmentHome.stopShimmer();
                    binding.shimmerItemMovieNowPlayingFragmentHome.setVisibility(View.GONE);

                    nowPlayingMovieAdapter.setmMovie_Now_Playing(movieModels_NowPlaying);
//                    }
                }

            }
        });
        movieListViewModel.searchMovieNowPlaying(1);


    }

}