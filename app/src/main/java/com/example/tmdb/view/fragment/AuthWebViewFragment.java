package com.example.tmdb.view.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentAuthWebViewBinding;
import com.example.tmdb.utils.SharedPrefence;
import com.example.tmdb.viewmodels.AuthViewModel;


public class AuthWebViewFragment extends Fragment {


    private FragmentAuthWebViewBinding binding;

    AuthViewModel authViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAuthWebViewBinding.inflate(inflater , container , false);

        authentication();
        onClick();

        return binding.getRoot();
    }
    private void authentication(){
        String request_token = SharedPrefence.saveToken(getContext()).getString("token" , "");
        binding.authWebView.setWebViewClient(new WebViewClient());
        binding.authWebView.loadUrl(Credentials.BASE_GRANTE_TOKEN_URL + request_token);

        WebSettings webSettings = binding.authWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void onClick(){
        binding.backarrow.setOnClickListener(view -> {
            Navigation.findNavController(view).navigate(R.id.profileFragment);
        });
    }
}