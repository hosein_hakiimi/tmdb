package com.example.tmdb.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.tmdb.R;
import com.example.tmdb.utils.AuthenticationClient;
import com.example.tmdb.utils.SharedPrefence;

public class AuthActivity extends AppCompatActivity {

    AuthenticationClient authenticationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        checkStatus();

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    private void checkStatus(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("main_request_token" , MODE_PRIVATE);
        String token = sharedPreferences.getString("main_token" , "");
        if (!token.equals("")){
            Intent i = new Intent(this , MainActivity.class);
            startActivity(i);
            finish();
        }
    }
}