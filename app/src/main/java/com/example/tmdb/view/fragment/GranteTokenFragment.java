package com.example.tmdb.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import com.example.tmdb.R;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentGranteTokenBinding;

public class GranteTokenFragment extends Fragment {

    private FragmentGranteTokenBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentGranteTokenBinding.inflate(inflater , container , false);

        setWebViewGrant();
        onClick();

        return binding.getRoot();
    }
    private void setWebViewGrant(){
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("request_token" , Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token" , "");

        binding.webviewGranteToken.setWebViewClient(new WebViewClient());
        binding.webviewGranteToken.loadUrl(Credentials.BASE_GRANTE_TOKEN_URL + token);

        WebSettings webSettings = binding.webviewGranteToken.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    private void onClick(){
        binding.directtohome.setOnClickListener(view -> {
            Navigation.findNavController(view).navigate(R.id.homeFragment);
        });
    }
}