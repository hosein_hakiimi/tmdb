package com.example.tmdb.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.tmdb.R;
import com.example.tmdb.Validation;
import com.example.tmdb.databinding.FragmentLogInBinding;
import com.example.tmdb.utils.SharedPrefence;
import com.example.tmdb.viewmodels.AuthViewModel;
import com.google.android.material.textfield.TextInputEditText;

public class LogInFragment extends Fragment {

    private FragmentLogInBinding binding;
    private AuthViewModel authViewModel;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLogInBinding.inflate(inflater , container , false);

        passwordSetVisibility();
        observToken();
        onClick();

        return binding.getRoot();
    }

    private void observToken(){
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);

        authViewModel.getNewToken();
        authViewModel.newToken(getContext());
    }

    private void onClick(){
        binding.buttonLogInFragmentLogIn.setOnClickListener(view -> {
            validation(getContext() , binding.inputTextUsernameFragmentLLogin , binding.inputTextPasswordFragmentLLogin);
        });
    }

    public void validation(Context context, TextInputEditText editTextUsername, TextInputEditText editTextPassword) {
        String username = editTextUsername.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (username.equals("")) {
            Toast.makeText(context, "Please enter your username", Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
        } else {
            authViewModel.login(context , username , password);
        }
        editor = SharedPrefence.profileData(getContext()).edit();
        editor.putString("username" , username);
        editor.apply();
    }

    private void passwordSetVisibility(){
        binding.imageviewCantSeePasswordFragmentLogIn.setOnClickListener(view -> {
            binding.imageviewSeePasswordFragmentLogIn.setImageResource(R.drawable.ic_see);
            binding.inputTextPasswordFragmentLLogin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            binding.imageviewSeePasswordFragmentLogIn.setVisibility(View.VISIBLE);
            binding.imageviewCantSeePasswordFragmentLogIn.setVisibility(View.GONE);
        });
        binding.imageviewSeePasswordFragmentLogIn.setOnClickListener(view -> {
            binding.imageviewSeePasswordFragmentLogIn.setImageResource(R.drawable.ic_see);
            binding.inputTextPasswordFragmentLLogin.setTransformationMethod(PasswordTransformationMethod.getInstance());
            binding.imageviewCantSeePasswordFragmentLogIn.setVisibility(View.VISIBLE);
            binding.imageviewSeePasswordFragmentLogIn.setVisibility(View.GONE);
        });

    }
}


