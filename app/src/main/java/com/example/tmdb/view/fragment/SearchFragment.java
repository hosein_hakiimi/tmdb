package com.example.tmdb.view.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tmdb.controller.adapter.SearchAdapter;
import com.example.tmdb.databinding.FragmentSearchBinding;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.viewmodels.MovieListViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class SearchFragment extends Fragment {


    private FragmentSearchBinding binding;


    private MovieListViewModel movieListViewModel;

    private SearchAdapter searchAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater , container , false);

        movieListViewModel =  new ViewModelProvider(this).get(MovieListViewModel.class);

        // calling the observers
        ObserveAnyChange();
        configureRecycleview();
        SetUpSearchView();

        return binding.getRoot();
    }


    // the observer observ data change
    // Observing any data change
    private void ObserveAnyChange(){

        movieListViewModel.getMovie().observe(getViewLifecycleOwner(), new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels) {
                // Observing for any data change
                if (movieModels != null){
                    for (MovieModel movieModel : movieModels){
//                        Log.v("Tagy" , "onChanged:" + movieModel.getTitle());
                        searchAdapter.setmMovies(movieModels);
                    }
                }

            }
        });

    }

    // 5- intializing recyclerview $ adding data to it
    private void configureRecycleview(){

        searchAdapter = new SearchAdapter(getContext());
        binding.recyclerviewActivityMain.setAdapter(searchAdapter);
        binding.recyclerviewActivityMain.setLayoutManager(new LinearLayoutManager(getContext()));

        // recyclerview pagination
        // loading next page of api response
        binding.recyclerviewActivityMain.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1)){
                    // Here we need to display the next search result on the next page of api
                    movieListViewModel.searchNextPage();
                }
            }
        });

    }

    // getting data from search & query the api to get the result (movie)
    private void SetUpSearchView(){
        binding.searchviewMain.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                movieListViewModel.searchMovieApi(
                        // the search string have gotten from searchview
                        query,
                        1
                );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }
}