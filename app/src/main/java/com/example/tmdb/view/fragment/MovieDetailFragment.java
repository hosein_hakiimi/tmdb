package com.example.tmdb.view.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.bumptech.glide.Glide;
import com.example.tmdb.R;
import com.example.tmdb.Service;
import com.example.tmdb.controller.adapter.GenreMovieAdapter;
import com.example.tmdb.controller.adapter.ImagesMovieAdapter;
import com.example.tmdb.controller.services.Credentials;
import com.example.tmdb.databinding.FragmentMovieDetailBinding;
import com.example.tmdb.models.MovieDetail.BackdropsImage;
import com.example.tmdb.models.MovieDetail.GenreMovie;
import com.example.tmdb.models.MovieDetail.ImagesMovie;
import com.example.tmdb.models.MovieDetail.LanguageMovie;
import com.example.tmdb.models.MovieDetail.MovieModel;
import com.example.tmdb.models.MovieDetail.ProductionCompany;
import com.example.tmdb.models.MovieDetail.ProductionCountryMovie;
import com.example.tmdb.utils.SharedPrefence;
import com.google.android.material.appbar.AppBarLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    private FragmentMovieDetailBinding binding;

    DetailFragment detailFragment;
    RecommendationFragment recommendationFragment;

    int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false);

        getMovieData();

//        binding.collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.white));
//        binding.collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.black));
//        binding.collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.transparent));

        setTabLayout();

        binding.appbar.addOnOffsetChangedListener(this);

        onClick();


        return binding.getRoot();
    }


    private void onClick() {
        binding.buttonback.setOnClickListener(view -> {
            Navigation.findNavController(view).popBackStack();
        });
        binding.backbutton.setOnClickListener(view -> {
            Navigation.findNavController(view).popBackStack();
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
            //collapsed
            binding.relToolbar.setVisibility(View.VISIBLE);
            binding.relToolbar.setAlpha(0.0f);

            binding.relToolbar.animate()
                    .alpha(1.0f)
                    .setDuration(2000);
            binding.buttonback.setVisibility(View.GONE);
        } else if (verticalOffset == 0) {
            //expanded
            binding.relToolbar.setVisibility(View.GONE);
            binding.buttonback.setVisibility(View.VISIBLE);
        } else {
            // while collapsing or expanding
            binding.relToolbar.setVisibility(View.VISIBLE);
            binding.buttonback.setVisibility(View.GONE);
        }
    }


    private void getMovieData() {

//        int movie_id = SharedPrefence.movieId(getContext()).getInt("movie_id" , 0);
        int movie_id = getArguments().getInt("movie_id");


        Call<MovieModel> call = Service.getApiService().getMovieDetail(movie_id, Credentials.API_KEY);
        call.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(@NotNull Call<MovieModel> call, @NotNull Response<MovieModel> response) {


                Glide.with(getContext()).load(Credentials.BASE_IMAGE_URL + response.body().getPoster_path()).into(binding.imageviewPosterFragmentMovieDetial);


            }

            @Override
            public void onFailure(Call<MovieModel> call, Throwable t) {

            }
        });


    }

    private void setTabLayout() {
        detailFragment = new DetailFragment();
        recommendationFragment = new RecommendationFragment();


        binding.tabLayoutFragmentMovieDetail.setupWithViewPager(binding.viewpagerFragmentMovieDetail);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(requireActivity().getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putInt("id_movie", getArguments().getInt("movie_id"));
        detailFragment.setArguments(bundle);
        recommendationFragment.setArguments(bundle);


        viewPagerAdapter.addFragment(detailFragment, "Detail");
        viewPagerAdapter.addFragment(recommendationFragment, "Recommendation");

        binding.viewpagerFragmentMovieDetail.setAdapter(viewPagerAdapter);


    }

    private static class ViewPagerAdapter extends FragmentStatePagerAdapter implements TabHost.OnTabChangeListener {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitle.add(title);
            notifyDataSetChanged();
        }

        @NonNull
        @NotNull
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }

        @Override
        public void onTabChanged(String tabId) {

        }

        @Override
        public int getItemPosition(@NonNull @NotNull Object object) {
            if (getItemPosition(object) >= 0) {
                return getItemPosition(object);
            } else {
                return POSITION_NONE;
            }
        }
    }



}
