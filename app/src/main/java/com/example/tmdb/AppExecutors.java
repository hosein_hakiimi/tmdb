package com.example.tmdb;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

// executors are used for background tasks

public class AppExecutors {
    //singlton pattern
    private static AppExecutors instance;

    public static AppExecutors getInstance(){
        if(instance == null){
            instance = new AppExecutors();
        }
        return instance;
    }

    // getting data from retrofit from api and go back to the web service and store them in live data using background thread (scheduledexecutor)
    // and passing it to repository and the view model and finally to the UI
    private final ScheduledExecutorService mNetworkIO = Executors.newScheduledThreadPool(3);

    public ScheduledExecutorService getmNetworkIO() {
        return mNetworkIO;
    }

}
