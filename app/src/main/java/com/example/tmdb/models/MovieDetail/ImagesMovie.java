package com.example.tmdb.models.MovieDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImagesMovie {

    @SerializedName("backdrops")
    @Expose
    private List<BackdropsImage> backdropsImages;

    public List<BackdropsImage> getBackdropsImages() {
        return backdropsImages;
    }

    public void setBackdropsImages(List<BackdropsImage> backdropsImages) {
        this.backdropsImages = backdropsImages;
    }
}
