package com.example.tmdb.models;

import com.example.tmdb.models.MovieDetail.MovieModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// this class is for requesting or for single movie request
public class MovieResponse {
    // 1- finding the movie object
    @SerializedName("results")
    @Expose
    private MovieModel movieModel;


    // getter and to string method
    public MovieModel getMovie(){
        return movieModel;
    }

    @Override
    public String toString() {
        return "MovieResponse{" +
                "movieModel=" + movieModel +
                '}';
    }
}
