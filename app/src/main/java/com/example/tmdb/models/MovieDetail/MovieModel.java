package com.example.tmdb.models.MovieDetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MovieModel implements Serializable {

    @SerializedName("id")
    @Expose()
    private int id;
    @SerializedName("title")
    @Expose()
    private String title;
    @SerializedName("poster_path")
    @Expose()
    private String poster_path;
    @SerializedName("release_date")
    @Expose()
    private String release_date;
    @SerializedName("movie_id")
    @Expose()
    private int movie_id;
    @SerializedName("vote_average")
    @Expose()
    private float vote_average;
    @SerializedName("overview")
    @Expose()
    private String overview;
    @SerializedName("original_language")
    @Expose()
    private String original_language;
    @SerializedName("backdrop_path")
    @Expose()
    private String backdrop_path;
    @SerializedName("status")
    @Expose()
    private String status;
    @SerializedName("vote_count")
    @Expose()
    private int vote_count;
    @SerializedName("production_companies")
    @Expose()
    private List<ProductionCompany> productionCompanies;
    @SerializedName("spoken_languages")
    @Expose()
    private List<LanguageMovie> languageMovies;
    @SerializedName("genres")
    @Expose()
    private List<GenreMovie> genreMovies;
    @SerializedName("production_countries")
    @Expose()
    private List<ProductionCountryMovie> productionCountryMovies;



    public MovieModel(String original_language) {
        this.original_language = original_language;
    }


    public MovieModel(String title, String poster_path, String release_date, int movie_id, float vote_average, String overview, String original_language, String backdrop_path) {
        this.title = title;
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.movie_id = movie_id;
        this.vote_average = vote_average;
        this.overview = overview;
        this.original_language = original_language;
        this.backdrop_path = backdrop_path;
    }

    protected MovieModel(Parcel in) {
        title = in.readString();
        poster_path = in.readString();
        release_date = in.readString();
        movie_id = in.readInt();
        vote_average = in.readFloat();
        overview = in.readString();
        original_language = in.readString();
        poster_path = in.readString();
    }




    public List<ProductionCountryMovie> getProductionCountryMovies() {
        return productionCountryMovies;
    }

    public void setProductionCountryMovies(List<ProductionCountryMovie> productionCountryMovies) {
        this.productionCountryMovies = productionCountryMovies;
    }

    public List<GenreMovie> getGenreMovies() {
        return genreMovies;
    }

    public void setGenreMovies(List<GenreMovie> genreMovies) {
        this.genreMovies = genreMovies;
    }

    public List<LanguageMovie> getLanguageMovies() {
        return languageMovies;
    }

    public void setLanguageMovies(List<LanguageMovie> languageMovies) {
        this.languageMovies = languageMovies;
    }

    public List<ProductionCompany> getProductionCompanies() {
        return productionCompanies;
    }

    public void setProductionCompanies(List<ProductionCompany> productionCompanies) {
        this.productionCompanies = productionCompanies;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String movie_overview) {
        this.overview = movie_overview;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    @Override
    public String toString() {
        return "MovieModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", poster_path='" + poster_path + '\'' +
                ", release_date='" + release_date + '\'' +
                ", movie_id=" + movie_id +
                ", vote_average=" + vote_average +
                ", overview='" + overview + '\'' +
                ", original_language='" + original_language + '\'' +
                ", backdrop_path='" + backdrop_path + '\'' +
                ", vote_count=" + vote_count +
                ", productionCompanies=" + productionCompanies +
                '}';
    }
}
