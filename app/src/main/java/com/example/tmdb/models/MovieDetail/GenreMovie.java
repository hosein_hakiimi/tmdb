package com.example.tmdb.models.MovieDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenreMovie {

    @SerializedName("id")
    @Expose()
    int id;
    @SerializedName("name")
    @Expose()
    String name;


    public GenreMovie(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
