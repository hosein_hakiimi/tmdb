package com.example.tmdb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Response;

public class RequestToken {
    @SerializedName("success")
    @Expose
    boolean success;
    @SerializedName("expires_at")
    @Expose
    String expires_at;
    @SerializedName("request_token")
    @Expose
    String request_token;
    @SerializedName("status_code")
    @Expose
    int status_code;
    @SerializedName("status_message")
    @Expose
    String status_message;



    public RequestToken(boolean success, String expires_at, String request_token, int status_code, String status_message) {
        this.success = success;
        this.expires_at = expires_at;
        this.request_token = request_token;
        this.status_code = status_code;
        this.status_message = status_message;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }

    public String getRequest_token() {
        return request_token;
    }

    public void setRequest_token(String request_token) {
        this.request_token = request_token;
    }
}
