package com.example.tmdb.models.MovieDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageMovie {

    @SerializedName("english_name")
    @Expose()
    String english_name;
    @SerializedName("iso_639_1")
    @Expose()
    String iso_639_1;
    @SerializedName("name")
    @Expose()
    String name;

    public LanguageMovie(String english_name, String iso_639_1, String name) {
        this.english_name = english_name;
        this.iso_639_1 = iso_639_1;
        this.name = name;
    }

    public String getEnglish_name() {
        return english_name;
    }

    public void setEnglish_name(String english_name) {
        this.english_name = english_name;
    }

    public String getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
