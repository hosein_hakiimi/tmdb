package com.example.tmdb.models.MovieDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackdropsImage {

    @SerializedName("aspect_ratio")
    @Expose()
    Float aspect_ratio;
    @SerializedName("iso_639_1")
    @Expose()
    String iso_639_1;
    @SerializedName("file_path")
    @Expose()
    String file_path;
    @SerializedName("vote_average")
    @Expose()
    Float vote_average;
    @SerializedName("vote_count")
    @Expose()
    Float vote_count;
    @SerializedName("height")
    @Expose()
    int height;

    public Float getAspect_ratio() {
        return aspect_ratio;
    }

    public void setAspect_ratio(Float aspect_ratio) {
        this.aspect_ratio = aspect_ratio;
    }

    public String getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public Float getVote_average() {
        return vote_average;
    }

    public void setVote_average(Float vote_average) {
        this.vote_average = vote_average;
    }

    public Float getVote_count() {
        return vote_count;
    }

    public void setVote_count(Float vote_count) {
        this.vote_count = vote_count;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
